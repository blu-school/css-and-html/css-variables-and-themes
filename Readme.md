# CSS Variables and themes
This project is created to show you how you can use `CSS variables` in your project with or without existing `SCSS variables`.

Meeting recording avaliable here: [MEETING](https://blubitoag-my.sharepoint.com/:v:/g/personal/p_ivanova_blubito_com/EfKQ_FQ8XmNKi4DqU4G-aSEBxTmDo8RYSyVArhZVIBF3TA)

## Run project
```
1) Install dependencies
npm install
yarn install

2) Start the project
npm run dev
yarn dev
```

## Files
Under `/src` you'll find all the files you need for this project. 

### var-examples.scss
Examples on using and overwriting SCSS and CSS variables

### styles.scss
Examples on using CSS variables inside a component and how to use them for less code.

### theme.scss & main.js
Example on basic theme setup and changing it via JS


## Links
[SASS Variables](https://sass-lang.com/documentation/variables)

[Using CSS custom properties (variables)](https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties)

[CSS Variables in 100 Seconds](https://www.youtube.com/watch?v=NtRmIp4eMjs&ab_channel=Fireship)

[CSS Variables Videos](https://www.youtube.com/watch?v=PHO6TBq_auI&ab_channel=KevinPowell)
