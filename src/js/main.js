export default getColors = () => {
    const primary = document.getElementById("primary-color").value;
    const secondary = document.getElementById("secondary-color").value;
    console.log('getColors', primary, secondary)

    let root = document.documentElement;
    root.style.setProperty('--color-primary', primary);
    root.style.setProperty('--color-secondary', secondary);
}